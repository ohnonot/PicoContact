The settings are stored in a `config.json` file. This file shouldn't be edited manually, as a functional configuration panel can be displayed using :

```php
echo $p01contact->panel();
```

This panel is accessible in the supported CMS, see [[GetSimple plugin#configuration]] or [[Pico CMS plugin#configuration]].

# General configuration

| Setting | Description |
|--|--|
| *Default emails* | One ore more email addresses, separated by commas, that will receive mails from every forms.
| *Language* | Define the global language used in forms. See [translation](translation).
| *Messages minimum length* | Minimum number of characters to write in `message` fields to be valid.
| *Default parameters* | Parameters used for default forms. See [syntax](syntax).
| *Separator* | Character separating parameters in the [syntax](syntax). Comma by default. You may want to use a less-common character if you need commas inside the parameters.
| *Logs number* | Maximum number of entries to keep on logs.

## Default language and form language

The default global language is English, or may be set trough code.  
When used as a CMS plugin, the global default language will match the system one.

Each form can override this setting and be defined to a specific language.

```php
$p01contact = new P01contact();
$p01contact->default_lang = 'ru';

$form = new P01contactForm($p01contact);
$form->lang = 'fr';
```

In tags [syntax](syntax) : 

    (% contact fr %)

# Fields checklists

These options allows to define required or prohibited terms for specific fields types.

The first field indicates the field type, for example `email`, the second input define the rule type : 

- `Blacklist` : prohibited values that must not be present in the given field.
- `Whitelist` : possible values required for the given field.

The last field contains the list of terms, separated by commas.

# Security

These options allow to configure the security mechanisms included by default or to setup additional measures.

| Setting | Description |
|--|--|
| *Use hidden honeypot field* | Add to every forms a hidden field that should not be filled. Humans won't see it, but most spam bots will fell in the trap.
| *Minimum time after page load* | Minimum number of seconds between a page load and a form submission. Fellow humans need at least a few seconds to read, fill and submit a form. Robots don't.
| *Minimum time between two submissions* | Minimum number of seconds between two form submissions.
| *Maximum submissions by hour* | Maximum number of submissions every hour.

The system will also detect and prevent the user to send again a submitted form (ex. reloading the page).

The native anti-spam and anti-error mechanisms should prevent all undesirable submissions. If you need a higher level of security you can insert a Google reCaptcha by using a `captcha` field and defining the public and secret keys.

# Debug mode

These settings may be useful for debugging.

| Setting | Description |
|--|--|
| *Disable all forms* | Disable mail sending, without hiding the contact forms.
| *Debug mode* | Disable mail sending, display p01contact health checks, data structures, data sent by POST and the email that would have been sent. Don't active that on production website!

The debug data can be shown with : 

```php
$p01contact->debug();
```

For CMS, see [[GetSimple plugin#debug]] or  [[Pico CMS plugin#debug]].