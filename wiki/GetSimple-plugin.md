[p01contact](https://github.com/nliautaud/p01contact) is natively a plugin for [GetSimple CMS](http://get-simple.info).

## Installation

Download or clone the repository, then place the `p01-contact/` directory and the file `p01-contact_gs.php` in `plugins/`.

## Usage in pages

Just write tags in your pages according to the [syntax](syntax).

```
This is a default contact form :
    
(% contact %)
    
Simple.
```

Reminds you to fill the Meta Description accessible in page options. If you don't, GetSimple will show the tag source in the source code of the output page.

## Usage in themes and templates

You can use it in components, themes or templates by manipulating the variable `$p01contact`, already initialized. For example, to add a default contact form in your sidebar :

```php
<?php
get_component('sidebar');
echo $p01contact->parse('(% contact %)');
?>
```

## Configuration

The [plugin settings](settings) are accessible in the GetSimple administration.

![](https://github.com/nliautaud/p01contact/blob/master/images/capture_getsimple_settings.png?raw=true)

## Debug

When *Debug* is enabled in the [plugin settings](settings), a debug report is shown before the pages.
