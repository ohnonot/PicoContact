Emails are generated in UTF-8 HTML, with a text fallback for older clients.

The subject and person name/email will be defined by the fields `subject`, `name` and `email`. If not found, `Anonymous` and `(no subject)` are used.

In addition to every fields information, the mail includes the domain name, the path to the page, IP, date and time.

The language of the email is determined by the language settings. See [[Localization (i18n)]].

|Example of mail received in Gmail|
|:-:|
|![](https://github.com/nliautaud/p01contact/blob/master/images/capture_email.png?raw=true)|