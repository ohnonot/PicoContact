p01contact supports localization of the forms, emails, messages and administrative infos.

Bundled languages : 

> English, French, Italian, Portuguese  
> Bulgarian, Czech, Danish, Dutch, German, Georgian / Kartuli, Lithuanian, Macedonian,  
> Norwegian, Polish, Romanian, Russian, Serbian, Slovak, Spanish and Swedish.  
> [(see language files)](https://github.com/nliautaud/p01contact/tree/master/p01-contact/lang)

Languages are defined by their [ISO 639-1](https://wikipedia.org/wiki/ISO_639-1) two-letters code.

The language of default forms, administrative panels and received mails can be defined trough code with `P01contact->default_lang`. This variable is set to the CMS language when used as a plugin. This value can be overrided in the [settings](settings) to one of the available translations.

Additionally each form can be called in a distinct language, for example for a multi-language site, by filling in the language parameter. For example in [tags syntax](syntax) : 

    (% contact fr %)

Some languages files are not up to date for version 1 and 1.1, see [Translations for v1](https://github.com/nliautaud/p01contact/issues/18).

## Adding a language, improving translations

The languages files are located in the `lang/` directory.

Each file is named as a [ISO 639-1](https://wikipedia.org/wiki/ISO_639-1) two-letters code, and contains a list of strings defined by a *string key*. For example, in the english file, the key `sent` would be associated to the string `Email sent`.

The two main files that should be used as reference to populate the other languages are the `English (en)` and `French (fr)` ones.

The top of the files should contain authors, modified date and compatibility information. Credit yourself here.

The files are in `YAML`. If the sentence is using reserved characters like `:`, the whole string should be wrapped in double quotes `"`.  You should validate your file using a YML validator, for example https://codebeautify.org/yaml-validator.

### On Github

- Browse to https://github.com/nliautaud/p01contact/tree/master/p01-contact/lang
- Open a file and use the *Edit this file* button, or create a new file with *Create a new file*.
- Make changes, describe your modifications and *Commit changes*.

or

- Fork the project, commit your changes and create a Pull request.