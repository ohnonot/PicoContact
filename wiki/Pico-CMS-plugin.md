[p01contact](https://github.com/nliautaud/p01contact), named *Pico Contact*, is natively a plugin for [Pico CMS](http://picocms.org).

## Installation

Download or clone the repository, then place the `p01-contact/` directory in `plugins/`, and rename it `PicoContact`.

## Usage in pages

Just write tags in your pages according to the [syntax](syntax).

```markdown
---
Title: My contact page
Description: Contact me
---

This is a default contact form :
    
(% contact %)
    
Simple.
```

## Usage in themes and templates

The `contact` Twig function creates contact forms. Without parameters, it will output a default form as defined in the [settings](settings). Parameters can be added to create form-specific structures (see [syntax](syntax)) and localization.

```twig
{{ contact() }}                   output the default form
{{ contact('parameters') }}       custom parameters
{{ contact('fr', 'parameters') }} custom parameters and form-specific language
{{ contact('fr', null) }}         default form with form-specific language
```

## Configuration

A functional configuration panel to manage the [plugin settings](settings) can be shown with the `(% contact_admin_config %)` tag and `contact_admin` Twig function.

```
(% contact_admin_config %)     in pages
{{ contact_admin('config') }}  in themes
```

> This panel shouldn't be accessible in production by visitors or not trusted users. You can restrict this view to logged administrators by using the [Pico Users Plugin](https://github.com/nliautaud/pico-users).

![](https://github.com/nliautaud/p01contact/blob/master/images/capture_pico_settings.png?raw=true)

## Debug

If the *Debug* option is enabled in the [plugin settings](settings), the following tag and twig function will output a debug report.

```
(% contact_admin_debug %)      in pages
{{ contact_admin('debug') }}   in themes
```

> This panel shouldn't be accessible in production by visitors or not trusted users. You can restrict this view to logged administrators by using the [Pico Users Plugin](https://github.com/nliautaud/pico-users).