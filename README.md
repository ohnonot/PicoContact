# p01contact

Create contact forms by writing simple tags.

- Supports any fields types
- Powerful [textual syntax][syntax]
- Generates [comprehensive mails][emails]
- UTF-8, [localized and multilingual][i18n]
- Automatic [security measures]
- Integrated [settings editor][settings]
- Debug reports and [submission logs][logs]
- Plugin for [Pico CMS][Pico CMS plugin]
 
## Installation

For [Pico CMS], place the `PicoContact` directory in `/plugins/`, or clone directly into `/plugins/PicoContact`

*Compatibility : PHP 5.4+*

## Usage as a plugin

Just write tags in your pages.

```
This is a default contact form :

(% contact %)

Simple.
```

Follow the [syntax] to create custom forms.

```
(% contact en :
    subject => A locked subject,
    radio "I'd like to contact you" = a little | a lot |: passionately,
    select "Department" (the floor you look for) = Silly walks :| Strange things,
    email!,
    message =< Bla bla placeholder,
    checkbox! "I'm in control",
    askcopy
%)
```

Details about usage as a plugin can be found in the [wiki] :

## Usage as a PHP script

The simplest method is to include the script, create a new instance and parse strings containing tags using the [syntax].

```php
include 'p01-contact/P01contact.php';

$p01contact = new P01contact();

$content = 'This is a default contact form : (% contact %)'
$content = $p01contact->parse($content);
```

[Pico CMS]: http://picocms.org
[Pico CMS plugin]: https://framagit.org/ohnonot/PicoContact/wiki/Pico-CMS-plugin.md
[wiki]: https://framagit.org/ohnonot/PicoContact/wiki/Home.md
[syntax]: https://framagit.org/ohnonot/PicoContact/wiki/Syntax.md
[settings]: https://framagit.org/ohnonot/PicoContact/wiki/Settings.md
[security measures]: https://framagit.org/ohnonot/PicoContact/wiki/Settings.md#security
[i18n]: https://framagit.org/ohnonot/PicoContact/wiki/Localization-(i18n).md
[emails]: https://framagit.org/ohnonot/PicoContact/wiki/Emails.md
[logs]: https://framagit.org/ohnonot/PicoContact/wiki/Logs.md
